﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curacion : MonoBehaviour, IPowerUps
{
    public Transform Caja;
    public float velocidadRotacion = 180f;
    public int vida = 10;

    int IPowerUps.queEs()
    {
        return (int)IDPowerUps.VIDA;
    }

    int IPowerUps.queHace()
    {
        return vida;
    }

    void Update()
    {
        Caja.Rotate(Vector3.up * velocidadRotacion * Time.deltaTime);
    }
}
