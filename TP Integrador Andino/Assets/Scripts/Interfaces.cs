﻿interface IDaño
{
    bool HacerDaño(int vld, bool esJugador);
}
interface IPowerUps
{
    int queEs();
    int queHace();
}
public enum IDPowerUps
{
    VIDA = 0, BALAS = 1
}