﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlMenu : MonoBehaviour
{

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void Jugar()
    {
        SceneManager.LoadScene("TP integrador");
    }
    public void Salir()
    {       
        Application.Quit();
    }
}
