﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlBot : MonoBehaviour, IDaño
{
    public GameObject[] puntosReaparicion;
    public GameObject[] powerUp;
    int tiposPowerUp;

    public Transform objetivo;
    public Transform arma;
    public float distanciaDisparo = 10f;
    public float intervaloDisparo = 2f;
    float tiempoDisparo;
    float distanciaObjetivo;
    public int vida = 20;
    public float distanciaSeguimiento = 3f;
    public float intervaloSeguimiento = 2f;
    float tiempoSeguimiento;
    NavMeshAgent agente;
    public ParticleSystem particulas;
    WaitForSeconds esperar;
    GameManager gameManager;
    public AccionesJugador ScriptJugador;
    public CanvasControl Canvas;
    public int puedeMorir = 0;
    void Start ()
    {
        agente = GetComponent<NavMeshAgent>();
        tiempoDisparo = intervaloDisparo;
        tiempoSeguimiento = intervaloSeguimiento;
        esperar = new WaitForSeconds(particulas.main.duration);
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        ScriptJugador = GameObject.Find("Jugador").GetComponent<AccionesJugador>();
        objetivo = GameObject.Find("Jugador").GetComponent<Transform>();
        Canvas = GameObject.Find("Canvas").GetComponent<CanvasControl>();
    }

    public bool HacerDaño(int vld, bool esJugador)
    {
        
        if(esJugador == true)
        {
            particulas.Play();
            vida -= vld;
            if(vida <=0 && puedeMorir == 0)
            {
               StartCoroutine (Muerte());               
            }
            return true;
        }
        return false;
    }

    IEnumerator Muerte()
    {
        puedeMorir = 1;
        gameManager.totalKills += 1;
        yield return esperar;
        Destroy(gameObject);
        Canvas.totalKills += 1;
        ScriptJugador.dinero = ScriptJugador.dinero + 30;
        CanvasControl.instance.añadirTxtDinero(ScriptJugador.dinero);
        CrearPowerUp();
        CanvasControl.instance.añadirTxtKills(Canvas.totalKills);
        CanvasControl.instance.añadirTxtMaxkills(gameManager.puntuacionMaxima);
    }
    
    void Update()
    {
        Vector3 posNoRot = new Vector3(objetivo.position.x, transform.position.y, objetivo.position.z);
        transform.LookAt(objetivo);
        distanciaObjetivo = Vector3.Distance(transform.position, objetivo.position);

        Seguimiento();

        ControlDisparo();
    }

    void ControlDisparo()
    {
        tiempoDisparo -= Time.deltaTime;
        if(tiempoDisparo < 0)
        {
            if(distanciaObjetivo < distanciaDisparo)
            {
                tiempoDisparo = intervaloDisparo;
                GameObject bala = ControladorObjetos.instance.GenerarBala(false);
                bala.transform.position = arma.position;
                bala.transform.LookAt(objetivo.position);
            }
        }
    }
    void Seguimiento()
    {
        tiempoSeguimiento -= Time.deltaTime;
        if(tiempoSeguimiento < 0)
        {
            if(distanciaObjetivo > distanciaSeguimiento)
            {
                agente.SetDestination(objetivo.position);
                agente.stoppingDistance = distanciaSeguimiento;
                tiempoSeguimiento = intervaloSeguimiento;
            }
        }
    }
    void CrearPowerUp()
    {
        int spawns = Random.Range(0, 3);
        tiposPowerUp = Random.Range(0, 2);
        Instantiate(powerUp[tiposPowerUp], puntosReaparicion[spawns].transform.position, puntosReaparicion[spawns].transform.rotation);
    }
}
