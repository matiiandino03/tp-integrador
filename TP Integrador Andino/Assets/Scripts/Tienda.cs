﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    public GameObject TiendaUI;
    public bool mostrar;
    public GameObject jugador;
    public AccionesJugador aa;    
    void Start()
    {
        aa = jugador.GetComponent<AccionesJugador>();       
    }


    void Update()
    {
        if (mostrar == true)
        {
            TiendaUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            aa.enabled = false;            
        }
        else
        {
            TiendaUI.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            aa.enabled = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                mostrar = !mostrar;
            }
        }
    }

    private void OnTriggerExit()
    {
        mostrar = false;
    }  
}


