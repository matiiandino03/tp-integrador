﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public float velocidad = 8f;
    public float duracionVida = 2f;
    float timerVida;
    public int ataque = 10;
    public bool disparoJugador;
  
    void OnEnable()
    {        
        timerVida = duracionVida;
    }

    
    void Update()
    {
        timerVida -= Time.deltaTime;
        if(timerVida <=0)
        {
            gameObject.SetActive(false);
        }
     
    }

    private void FixedUpdate()
    {
        transform.position += transform.forward * velocidad * Time.fixedDeltaTime;       
    }
    private void OnTriggerEnter(Collider other)
    {   
        bool borrar = true;
        IDaño daño = other.GetComponent<IDaño>();
        if(daño != null)
        {
            borrar = daño.HacerDaño(ataque,disparoJugador);
        }
        if (borrar == true)
        { 
        gameObject.SetActive(false);
        }
    }
}
