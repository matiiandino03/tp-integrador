﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anotacion : MonoBehaviour
{
    public ParticleSystem efectoCanasta;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Pelota"))
        {
            efectoCanasta.Play();

        }
    }
}
