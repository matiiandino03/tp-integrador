﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vidrio : MonoBehaviour
{
    public int vida = 20;
    Animator animator;
    WaitForSeconds esperar;
    void Start()
    {
        animator = GetComponent<Animator>();
        esperar = new WaitForSeconds(1);
    }


    void Update()
    {
        if (vida <= 0)
        {
            animator.SetBool("romper", true);
            StartCoroutine(desaparecer());
        }
    }
    IEnumerator desaparecer()
    {
        yield return esperar;
        Destroy(gameObject);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bala")
        {
            vida -= 10;
        }
    }
}
