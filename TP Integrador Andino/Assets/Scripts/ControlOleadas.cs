﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlOleadas : MonoBehaviour
{
    public Text txtRonda;
    public GameObject[] puntosReaparicion;
    public GameObject[] enemigos;
    public int contOleadas;
    public int oleada;
    int tiposEnemigos;
    public bool reapareciendo;
    private int enemigosVivos;
    private GameManager gameManager;
    public CanvasControl Canvas;
   
    void Start()
    {

        contOleadas = 2;
        oleada = 1;
        reapareciendo = false;
        enemigosVivos = 0;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        txtRonda = GameObject.Find("txtRonda").GetComponent<Text>();
        txtRonda.enabled = false;
        txtRonda = GameObject.Find("txtRonda").GetComponent<Text>();
        txtRonda.enabled = false;
        Canvas = GameObject.Find("Canvas").GetComponent<CanvasControl>();
    }

    
    void Update()
    {
        txtRonda.text = "Ronda " + oleada.ToString();

        if (reapareciendo == false && enemigosVivos == Canvas.totalKills)
        {
            StartCoroutine(CrearOleada(contOleadas));
        }
    }

    IEnumerator CrearOleada (int oleadaC)
    {
        reapareciendo = true;
        txtRonda.enabled = true;       
        yield return new WaitForSeconds(4);
        txtRonda.enabled = false;       
        for ( int i = 0; i < oleadaC; i++)
        {
            CrearEnemigo(oleada);
            yield return new WaitForSeconds(2);
        }
        oleada += 1;
        contOleadas += 2;
        reapareciendo = false;

        yield break;
    }

    void CrearEnemigo(int oleada)
    {
        int spawn = Random.Range(0, 4);
        if (oleada == 1)
        {
            tiposEnemigos = 1;
        }
        else if (oleada < 2)
        {
            tiposEnemigos = 1;
        }
        else
        {
            tiposEnemigos = Random.Range(0, 2);
        }

        Instantiate(enemigos[tiposEnemigos], puntosReaparicion[spawn].transform.position,puntosReaparicion[spawn].transform.rotation);
        enemigosVivos += 1;
    }
}
