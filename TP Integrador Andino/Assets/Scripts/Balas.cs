﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balas : MonoBehaviour, IPowerUps
{
    public Transform Caja;
    public float velocidadRotacion = 180f;
    public int balas = 12;

    int IPowerUps.queEs()
    {
        return (int)IDPowerUps.BALAS;
    }

    int IPowerUps.queHace()
    {
        return balas;
    }

    void Update()
    {
        Caja.Rotate(Vector3.up * velocidadRotacion * Time.deltaTime);
    }
}
