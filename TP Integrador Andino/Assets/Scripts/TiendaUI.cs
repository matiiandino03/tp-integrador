﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaUI : MonoBehaviour
{    
    public GameObject jugador;
    public AccionesJugador aa;
    public ContolJugador bb;
    public GameObject[] puntosReaparicion;
    public GameObject[] powerUp;
    int tiposPowerUp;
    void Start()
    {
        aa = jugador.GetComponent<AccionesJugador>();
        bb = jugador.GetComponent<ContolJugador>();
        
    }  
    void Update()
    {
        
    }
    public void Balas()
    {
        if (aa.dinero >= 600)
        {
            CrearPowerUp();
            aa.dinero = aa.dinero - 600;
            CanvasControl.instance.añadirTxtDinero(aa.dinero);
        }
                  
    }
    public void VidaMax()
    {
        if(aa.dinero >= 600)
        {
            aa.vidamax = aa.vidamax + 10;
            aa.dinero = aa.dinero - 600;
            CanvasControl.instance.añadirTxtDinero(aa.dinero);
        }
        
    }
    public void Velocidad()
    {
        if (aa.dinero >= 500)
        {
            bb.rapidezDesplazamiento = bb.rapidezDesplazamiento + 0.5f;
            aa.dinero = aa.dinero - 600;
            CanvasControl.instance.añadirTxtDinero(aa.dinero);
        }
            
    }

    void CrearPowerUp()
    {
        int spawns = Random.Range(0, 1);
        tiposPowerUp = Random.Range(0, 1);
        Instantiate(powerUp[tiposPowerUp], puntosReaparicion[spawns].transform.position, puntosReaparicion[spawns].transform.rotation);
    }
}

