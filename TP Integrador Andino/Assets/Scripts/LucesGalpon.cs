﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucesGalpon : MonoBehaviour
{
    public GameObject lucesGalpon;
    bool puedePrender;
    float distanciaAbrir = 2f;
    public int estado = 1;
    Transform objetivo;
    public float distanciaObjetivo;
    public GameObject palanca;
    Animator animator;
    WaitForSeconds esperar;
    public bool prender = false;
    void Start()
    {
        lucesGalpon.SetActive(false);
        objetivo = GameObject.Find("Interruptor").GetComponent<Transform>();        
        
        animator = palanca.GetComponent<Animator>();
        esperar = new WaitForSeconds(2);       
    }
   
    void Update()
    {        
        distanciaObjetivo = Vector3.Distance(transform.position, objetivo.position);

        if ( distanciaObjetivo < distanciaAbrir && Input.GetKeyDown(KeyCode.E) && estado == 1)
        {
            
            lucesGalpon.SetActive(true);
            estado = 2;
            animator.SetBool("Bajar", true);
            StartCoroutine(BajarSubir());
        }
       else if (distanciaObjetivo < distanciaAbrir && Input.GetKeyDown(KeyCode.E) && estado == 2)
        {
            lucesGalpon.SetActive(false);
            animator.SetBool("Bajar", true);
            StartCoroutine(BajarSubir());
            estado = 1;
        }
    }
    IEnumerator BajarSubir()
    {
        yield return esperar;
        animator.SetBool("Bajar", false);

    }
}
