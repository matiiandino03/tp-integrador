﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public float velocidad;
    public float angulo;
    public Vector3 direccion;
    public bool puedeAbrir;
    public bool estado;
    void Start()
    {
        angulo = transform.eulerAngles.y;
    }


    void Update()
    {
        if (Mathf.Round(transform.eulerAngles.y) != angulo)
        {
            transform.Rotate(direccion * velocidad);
        }
        if (Input.GetKeyDown(KeyCode.E) && puedeAbrir == true && estado == false)
            {
            angulo = 80;
            direccion = Vector3.up;
            estado = true;
            }
        else if(Input.GetKeyDown(KeyCode.E) && puedeAbrir == true && estado== true)
        {
            angulo = 0;
            direccion = Vector3.down;
            estado = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Jugador")
        {
            puedeAbrir = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Jugador")
        {
            puedeAbrir = false;
        }
    }
}
