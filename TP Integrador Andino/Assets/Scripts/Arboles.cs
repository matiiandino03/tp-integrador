﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arboles : MonoBehaviour
{
    public int vida = 30;    
    Animator animator;
    WaitForSeconds esperar;
    public ParticleSystem particulas;
    void Start()
    {
        animator =GetComponent<Animator>();
        esperar = new WaitForSeconds(particulas.main.duration);
    }

    
    void Update()
    {
        if (vida <= 0)
        {
            animator.SetBool("Caer", true);
            StartCoroutine(desaparecer());
            particulas.Play();
        }
    }   
    IEnumerator desaparecer()
    {
        yield return esperar;
        Destroy(gameObject);
             
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bala")
        {
            vida -= 10;
        }
    }
}
