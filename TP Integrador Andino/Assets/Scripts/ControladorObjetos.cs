﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorObjetos : MonoBehaviour
{
    struct infoBala
    {
        public GameObject prefab;
        public Bala scriptBala;
    }   
    public static ControladorObjetos instance;
    public GameObject balaPrefab;
    public int cantidadBalas = 5;    
    private List<infoBala> balas;
    
    void Awake()
    {
        instance = this;
        balas = new List<infoBala>(cantidadBalas);
        
        for (int i =0; i < cantidadBalas; i++)
        {
            infoBala BPrefab;
            BPrefab.prefab = Instantiate(balaPrefab);
            BPrefab.prefab.transform.SetParent(transform);
            BPrefab.prefab.SetActive(false);
            BPrefab.scriptBala = BPrefab.prefab.GetComponent<Bala>();
            balas.Add(BPrefab);          
        }
    }
    public GameObject GenerarBala(bool esJugador)
    {
        int totalBalas = balas.Count;
        for(int i = 0; i < totalBalas; i++)
        {
            if(!balas[i].prefab.activeInHierarchy)
            {
                balas[i].prefab.SetActive(true);
                balas[i].scriptBala.disparoJugador = esJugador;
                return balas[i].prefab;
            }
        }

        infoBala BPrefab;
        BPrefab.prefab = Instantiate(balaPrefab);
        BPrefab.prefab.transform.SetParent(transform);
        BPrefab.prefab.SetActive(true);
        BPrefab.scriptBala = BPrefab.prefab.GetComponent<Bala>();
        BPrefab.scriptBala.disparoJugador = esJugador;
        balas.Add(BPrefab);

        return BPrefab.prefab;
    }   
}
