﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AccionesJugador : MonoBehaviour , IDaño
{
    public GameObject pelota;   
    public float DistanciaPelota = 2f;
    public float fuerzaPelota = 250f;
    public bool esRecogible = true;
    public bool sosteniendoPelota = false;
    public LayerMask RecogibleLayer;
    public GameObject arma;
    RaycastHit hit1;
    Rigidbody pelotaRB;
    public Transform posArma;
    public Transform cam;
    RaycastHit hit;
    public LayerMask ignorarLayer;
    public int vida = 20;
    public int balas = 12;
    public GameObject efectoDaño;
    public float intervaloInvulnerable = 0.5f;
    float tiempoInvulnerable;
    WaitForSeconds esperar;
    Animator animator;
    public AudioClip Disparo;
    AudioSource fuente;
    public int vidamax = 100;
    public int dinero = 0;
    public GameObject pausa;
    public AccionesJugador script;
    public ContolJugador script2;

    private void Start()
    {
        pausa.SetActive(false);
        efectoDaño.SetActive(false);
        tiempoInvulnerable = 0.0f;
        CanvasControl.instance.añadirTxtVida(vida);
        CanvasControl.instance.añadirTxtBalas(balas);
        CanvasControl.instance.añadirTxtDinero(dinero);
        esperar = new WaitForSeconds(0.2f);
        pelotaRB = pelota.GetComponent<Rigidbody>();
        animator = arma.GetComponent<Animator>();
        fuente = GetComponent<AudioSource>();
        script = GetComponent<AccionesJugador>();
        script2 = GetComponent<ContolJugador>();
    }   
    private void Update()
    {      
        if(balas > 0 && Input.GetMouseButtonDown(0))
        {
            fuente.PlayOneShot(Disparo);
            animator.SetBool("Disparo", true);
            Vector3 dir = cam.TransformDirection(new Vector3(Random.Range(-0.005f, 0.005f), Random.Range(-0.005f, 0.005f), 1));          
            GameObject balaObj = ControladorObjetos.instance.GenerarBala(true);
            balaObj.transform.position = posArma.position;

            if(Physics.Raycast(cam.position, dir, out hit, Mathf.Infinity, ~ignorarLayer ))
            {
                balaObj.transform.LookAt(hit.point);
            }
            else
            { 
            Vector3 direccion = cam.position + dir * 10f;
            balaObj.transform.LookAt(direccion);
            }
            balas--;
            CanvasControl.instance.añadirTxtBalas(balas);
        }
        tiempoInvulnerable -= Time.deltaTime;       
        
        if (vida <= 0)
        {
            Perder();
        }
       if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
    }
     
    private void LateUpdate()
    {
        if (sosteniendoPelota == true)
        {
            pelota.transform.position = cam.position + cam.forward * DistanciaPelota;
            pelotaRB.useGravity = false;
            if (Input.GetKeyDown(KeyCode.E))
            {
                sosteniendoPelota = false;
                pelotaRB.useGravity = true;
                pelotaRB.AddForce(cam.forward * fuerzaPelota);
            }
        }
        else
        {
            if(Physics.Raycast(cam.position, cam.TransformDirection(Vector3.forward), out hit1, Mathf.Infinity, RecogibleLayer))
            {
                if(esRecogible == false)
                {
                    esRecogible = true;
                }
                if(esRecogible && Input.GetKeyDown(KeyCode.E))
                {
                    sosteniendoPelota = true;
                    pelotaRB.useGravity = false;
                    pelotaRB.velocity = Vector3.zero;
                    pelotaRB.angularVelocity = Vector3.zero;
                    pelota.transform.localRotation = Quaternion.identity;
                }
                else if (esRecogible == true)
                {
                    esRecogible = false;
                }
            }
        }
        animator.SetBool("Disparo", false);
    }
    public bool HacerDaño(int vld, bool esJugador)
    {
        
        if(esJugador == true)
        {
            return false;
        }
        else
        {
            if(tiempoInvulnerable <= 0)
            {
                vida -= vld;
                CanvasControl.instance.añadirTxtVida(vida);
                StartCoroutine(Efecto());
            }
        }
        
        return true;
    }

    IEnumerator Efecto()
    {
        efectoDaño.SetActive(true);
        yield return esperar;
        efectoDaño.SetActive(false);
        tiempoInvulnerable = intervaloInvulnerable;
    }

    private void OnTriggerEnter(Collider other)
    {
        IPowerUps powerUp = other.GetComponent<IPowerUps>();
        if(powerUp != null)
        {
            int res = powerUp.queHace();
            if(powerUp.queEs() == (int)IDPowerUps.VIDA && vida < vidamax)
            {
                vida += res;
                CanvasControl.instance.añadirTxtVida(vida);
            }
            else if (powerUp.queEs() == (int)IDPowerUps.BALAS)
            {
                balas += res;
                CanvasControl.instance.añadirTxtBalas(balas);
            }
            Destroy(other.gameObject);
        }
    }
    public void Perder()
    {
        SceneManager.LoadScene("Menu");       
    }
}
