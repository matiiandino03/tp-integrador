﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanvasControl : MonoBehaviour
{
    public Text txtVida;
    public Text txtBalas;
    public Text txtDinero;
    public Text txtKills;
    public Text txtRecord;
    public int totalKills;
    public static CanvasControl instance;
    GameManager gameManager;
    public int record;

    void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        añadirTxtKills(totalKills);
        añadirTxtMaxkills(gameManager.puntuacionMaxima);
        gameManager.totalKills = 0;
    }
    
    public void añadirTxtVida(int vld)
    {
        txtVida.text = "Vida: " + vld.ToString();
    }
    public void añadirTxtBalas(int vld)
    {
        txtBalas.text = "Balas: " + vld.ToString();
    }
    public void añadirTxtDinero(int vld)
    {
        txtDinero.text = "$" + vld.ToString();
    }
    public void añadirTxtMaxkills(int vld)
    {
        txtRecord.text = gameManager.puntuacionMaxima.ToString();
    }
    public void añadirTxtKills(int vld)
    {
        txtKills.text = "Kills: " + totalKills.ToString();
    }
}
