﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    Text txtKills;
    public int totalKills = 0;   

    Text txtRecord;
    public CanvasControl Canvas;
    //Persistencia
    public int puntuacionMaxima = 0;
    private string rutaArchivo;
    
    private void Awake()
    {
        rutaArchivo = Application.persistentDataPath + "/datos.dataaaaaaaaa";
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        Cargar();
    }
    void Start()
    {
        instance = this;
        txtKills = GameObject.Find("TotalKills").GetComponent<Text>();
        txtRecord = GameObject.Find("NumMaxKills").GetComponent<Text>();
        Canvas = GameObject.Find("Canvas").GetComponent<CanvasControl>();       
    }

    private void Update()
    {                
        if (totalKills > puntuacionMaxima)
        {
            Guardar();
        }
        Cargar();
    }
   
    public void Guardar()
    {        
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(rutaArchivo);

        DatosAGuardar datos = new DatosAGuardar();
        datos.puntuacionMaxima = totalKills;

        bf.Serialize(file, datos);

        file.Close();
    }

    void Cargar()
    {
        if (File.Exists(rutaArchivo))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(rutaArchivo, FileMode.Open);

            DatosAGuardar datos = (DatosAGuardar)bf.Deserialize(file);

            puntuacionMaxima = datos.puntuacionMaxima;

            file.Close();
        }
        else
        {
            
        }
    }
}
[System.Serializable]
class DatosAGuardar
{
    public int puntuacionMaxima;
}
